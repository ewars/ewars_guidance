# Reviewing active assignments

When your assignment has been approved:
1. You will also be able to view all active assignments in the Overview dashboard in the My Assignments box.

![main-image](assignments/image_14.png)
<span class="coordinates">1, 270,410,west;</span>

Notes:
- You will also receive an email confirmation when your assignment has been approved
