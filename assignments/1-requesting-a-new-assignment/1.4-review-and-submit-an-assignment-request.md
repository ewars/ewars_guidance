# Review and submit an Assignment request

Finally, in the Review form: 

1. Review the selections made in the previous forms
2. Press Request Assignment

![main-image](assignments/image_13.png)
<span class="coordinates">1, 140,353,west;2, 340,563,east;</span>

Notes:
- Once submitted an Account Administrator will review the request. You will receive a notification by email when it is approved or rejected. 

Related guidance:
- [Managing user assignment requests](#/task-management/2-new-user-assignments) 
