# Requesting a new Assignment

Before you can begin to submit reports, you need to request an assignment for the reporting form in the location(s) where you are working.

To request a new assignment:
1. Go to the Overview dashboard. 
2. Click on the + Request new button in the Assignments box.

![main-image](assignments/image_9.png)
<span class="coordinates">1, 6, 133, west; 2, 266, 635, east;</span>