# User Accounts
<strong>EWARS supports a number of different types of administrator and user
accounts. These provide access to different levels of functionality and
permissions the application.
</strong>   

Table of contents    <br><br>
[1. Types of user accounts](#/user-accounts/1-types-of-user-accounts)   
[2. Sign in to an exisiting user account](#/user-accounts/2-sign-in-to-an-existing-user-account)   
[3. Sign up for a new account](#/user-accounts/3-sign-up-for-a-new-account)   
[4. Resetting user password](#/user-accounts/4-resetting-user-password)