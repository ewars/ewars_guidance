'use strict';

module.exports = function(grunt) {
	var config = {
		pkg: grunt.file.readJSON('package.json'),
		sass: { // Task
			dist: { // Target
				options: { // Target options
					style: 'compressed',
					noCache: false
				},
				files: { // Dictionary of files
					'resources/src/css/style.css': 'resources/stylesheet/sass/main.scss' // 'destination': 'source'
				}
			}
		},
		cssmin: {
			target: {
				options: {
					sourceMap: true
				},
				files: {
					'resources/dist/css/style.min.css': 'resources/src/css/style.css'
				}
			}
		},
		ngAnnotate: {
			options: {
				singleQuotes: true
			},
			app: {
				files: {
					'resources/javascript/min-safe/router.config.js': ['resources/javascript/application/router.config.js'],
					'resources/javascript/min-safe/activeLink.directive.js': ['resources/javascript/application/activeLink.directive.js'],
					'resources/javascript/min-safe/menu.directive.js': ['resources/javascript/application/menu.directive.js'],
					'resources/javascript/min-safe/htmlParser.directive.js': ['resources/javascript/application/htmlParser.directive.js'],
					'resources/javascript/min-safe/fetchMarkdownFile.factory.js': ['resources/javascript/application/fetchMarkdownFile.factory.js'],
				}
			}
		},
		concat: {
			js: { //target
				src: [
					'node_modules/jquery/dist/jquery.js',
					'node_modules/showdown/dist/showdown.js',
					'node_modules/angular/angular.js',
					'node_modules/angular-ui-router/release/angular-ui-router.js',
					'node_modules/angular-sanitize/angular-sanitize.js',
					'resources/javascript/application/app.module.js',
					'resources/javascript/min-safe/*.js'
				],
				dest: 'resources/src/js/app.js'
			}
		},
		uglify: {
			js: { //target
				src: ['resources/src/js/app.js'],
				dest: 'resources/dist/js/script.min.js'
			}
		},
		copy: {
			main: {
				files: [{
					expand: true,
					cwd: 'resources/dist/',
					src: '**',
					dest: 'public/assets/'
				}],
			},
			css: {
				files: [{
					expand: true,
					cwd: 'resources/dist/css/',
					src: '**',
					dest: 'public/assets/css/'
				}]
			},
			js: {
				files: [{
					expand: true,
					cwd: 'resources/dist/js/',
					src: '**',
					dest: 'public/assets/js/'
				}]
			}
		},
		watch: {
			css: {
				files: '**/*.scss',
				tasks: ['sass', 'cssmin', 'copy:css']
			},
			js: {
				files: 'resources/javascript/**/*.js',
				tasks: ['ngAnnotate', 'concat', 'uglify', 'copy:js']
			}
		}
	};

	grunt.initConfig(config);
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-ng-annotate');
	grunt.loadNpmTasks('grunt-contrib-copy');

	grunt.registerTask('default', ['watch']);
}