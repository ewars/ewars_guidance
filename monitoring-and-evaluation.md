# Monitoring and Evaluation

<strong>Users need to be constantly aware of how they are performing and where improvements are needed. The M&E Auditor allows key performance indicators to be explored over time and at different geographic levels within EWARS.
</strong>   

Table of contents   <br><br>
[1. M&E Auditor](#/monitoring-and-evaluation/1-me-auditor)