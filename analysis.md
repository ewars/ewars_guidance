# Analysis

<strong>Data collected in EWARS needs to be presented in a simple, user-friendly way that makes it easy to rapidly analyse. This includes providing relevant, timely feedback to users to allow them to take them to take appropriate actions.
EWARS offers a range of analysis features to suit the needs of different users, from interactive exploration of data online to manual export to Excel.
</strong>   

Table of contents<br><br>
[1. Exporting data](#/analysis/1-exporting-data)   
[2. Exploring data](#/analysis/2-exploring-data)        