# Open Data Export 

You can access and manage reports using Data Export:

1. To open the Report Manager, go to the Main Menu
![image alt text](analysis/image_41.png)
2.  Select Analysis > Data Export

![image alt text](analysis/image_42.png)
