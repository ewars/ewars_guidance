# EWARS User Guidance
--------------------

### Getting Started   
* copy the **public** folder or its contents to your server, everything starts with the **index.html** file in the public folder.
* in **/public/assets/js/** folder edit the **env.js** file
```javascript
(function (window) {
  window.__env = window.__env || {};
  window.__env.apiUrl = 'PATH_TO_THE_MD_FOLDERS';
  window.__env.baseUrl = 'BASE_URL';
  window.__env.github = false;
  window.__env.enableDebug = false;
}(this));
```
* copy the rest of the folders to *GitHub* or anywhere outside of the ~~public~~ folder. 
* Point```javascript window.__env.apiUrl ``` to where the folders were copied to this is the most important variable to set.   
   * If api url points to a github api repo this ```javascript window.__env.github ``` option should be truthy

### Images & Arrows
the arrows are created dynamicly on the go based on a string of numbers provided after the image url.
```markdown
![main-image](user-accounts/image_0.png)
<span class="coordinates">1, 9, 193, west;</span>
```
so whats happening here, in the first line we have our image   
```markdown 
![main-image](user-accounts/image_0.png) 
```   
to have the coordinates find the image the first part of the string above needs to be any of the following:   
**main-image**, **image** or **mobile-image**

In the following line the coordiantes are being set:
```html
<span class="coordinates">1, 9, 193, west;</span>
```   
after ```<span class="coordinates">``` there are **3 numbers** and a **direction** separated by a **comma** ending in a **semi-colon**; this is how the coordinates are set.   
* the **first** number in the example "**1**, 9, 193, west;" is the number on the arrow in this case number **1** will be appended to the arrow.   
* the **Second** number "1, **9**, 193, west;" refers to the distance from the top edge of the image in pixles.   
* the **Third** number "1, 9, **193**, west;" refers to the distance from the left edige of the image in pixles.   
* the **diraction** at the end "1, 9, 193, **west;**" is the direction of the arrow
    * north = up
    * south = down
    * west = left
    * east = right   

Every set of arrow coordinates need to end in a semi-colon '**;**'