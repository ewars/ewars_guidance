# Task Management

<strong>EWARS needs to carefully manage a number of workflows within the application, such as the management of user registration, and the control over edits and deletions to reports.
This is managed through a task management system, available to users through the Tasks Dashboard.
</strong>   

Table of contents   <br><br>
[1. New User Registration](#/task-management/1-new-user-registration)   
[2. New User Assignments](#/task-management/2-new-user-assignments)   
[3. Report Amendments](#/task-management/3-report-amendments)   
[4. Report Deletion Requests](#/task-management/4-report-deletion-requests)   