# Open M&E Auditor

You can track key EWARS performance indicators using the M&E Auditor:

1. To open the M&E Auditor, go to the Main Menu
![image alt text](monitoring-and-evaluation/image_70.png)
2. Select Monitoring & Evaluation > M&E Auditor
![image alt text](monitoring-and-evaluation/image_71.png)
