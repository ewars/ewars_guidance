# Monitoring completeness

In the M&E Auditor:

1. Select Reporting Completeness as the Indicator
2. Select the Form you wish to track
3. Select the Start and End Date
4. The results will appear in the area below. They will be presented at all administrative levels within the EWARS Account. 
5. Use the navigation buttons to drill down into each location 

![main-image](monitoring-and-evaluation/image_72.png)
<span class="coordinates">1,150,61,north;2,150,190,north;3,150,290,north;4,200,610,west;5,266,-43,north;</span>