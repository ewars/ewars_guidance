# Assignments

<strong>An assignment defines (a) the type of reporting form and (b) the location where you are able to submit a report from. You may need to have a number of assignments, depending on the number of reporting forms that you need to submit and the number of locations where you are working.
Assignments must be requested and approved by an administrator before they are activated.
</strong>   

Table of contents   <br><br>
[1. Requesting a new Assignment](#/assignments/1-requesting-a-new-assignment)   
[2. Reviewing active assignments](#/assignments/2-reviewing-active-assignments)   