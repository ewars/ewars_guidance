# Synchronising EWARS Mobile

After submission, reports will be stored under Queued Reports whilst awaiting synchronisation with the EWARS database.

To synchronise the EWARS Mobile application:

1. Click on sync   
![mobile-image](ewars-mobile/image_82.png)
<span class="coordinates">1,600,600,west;3,350,600,west;</span>
2. A box will open to show the app is Syncing    
![mobile-image](ewars-mobile/image_81.png)
<span class="coordinates">2,500,500,west;</span>
3. After is has successfully Synced, the report will be moved to the Sent folder

Notes:
* When you synchronise you will automatically also receive any updates to the form (for example, changes to the list of diseases).
