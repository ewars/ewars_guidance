# Configuration of Android Phone

If you have been provided with a mobile phone from WHO to collect EWARS data, then you will need to set it up before it can be used. 

Notes:
- EWARS Mobile is currently only available for use on Android phones. In future, it will also be made available for iPhone.
- This guidance is based on a Samsung phone, but any Android phone can be used.
- You are free to install EWARS Mobile on your own personal phone if you wish