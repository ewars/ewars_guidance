# Using EWARS Mobile

To start a new report in EWARS Mobile:

1. Open EWARS Mobile to see the Assignments that you have been granted. This is the list of reports and locations where you can report from.  
2. Click on a report to open it in data entry mode

![mobile-image](ewars-mobile/image_73.png)
<span class="coordinates">1,741,329,north;2,446,741,west;</span>