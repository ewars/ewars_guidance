# Skip Payment info

EWARS Mobile is free of charge. You do not need to make any payments to use the application. 

1. Press Skip

![mobile-image](ewars-mobile/image_8.jpg)
<span class="coordinates">1,1256,441,west;</span>