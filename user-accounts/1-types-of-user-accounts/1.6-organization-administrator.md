# Organization Administrator

<table>
    <tr>
        <td valign="top"><strong>Example Title</strong></td>
        <td>NGO Health Coordinator<br>
UN Agency Health Coordinator 
</td>
    </tr>
    <tr>
        <td><strong>Type of profile</strong></td>
        <td>A person acting at a coordinator level within a partner Organization. <br>

In a given country, this user will be responsible for approving individual assignment requests for any new users within their own organisation. <br>

This user will also take an active role in activities for their organization such as:<br>
    - following up missing reports<br>
    - approving amendments to reports<br>
    - actively managing alerts<br>
    - creating new locations<br>
</td>
    </tr>
    <tr>
        <td><strong>Description of role</strong></td>
        <td>Administrative role, only has access to reports, data, analysis and other information which is directly created or generated from a user belonging to the same organization</td>
    </tr>
</table>