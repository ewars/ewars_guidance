# Global Administrator

<table>
    <tr>
        <td valign="top"><strong>Example title</strong></td>
        <td>Project Technical Lead</td>
    </tr>
    <tr>
        <td valign="top"><strong>Type of profile</strong></td>
        <td>A WHO epidemiologist at Headquarters and/or Regional level</td>
    </tr>
    <tr>
        <td valign="top"><strong>Description of role</strong></td>
        <td>Cross-instance administrative user with all-access privileges other than system-level.</td>
    </tr>
</table>