# As an administrator

 If you are an Adminsitrative user, you can obtain support from any of the following:

1. Write to your EWARS Account Administrator
2. Write to [support@ewars.ws](mailto:support@ewars.ws)
