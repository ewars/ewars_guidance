# As a reporting user

If you are a reporting user, you can obtain support from any of the following:

1. Write to the Location Adminstrator in the nearest location to where you are working
2. Write to your Organisation Administrator
3. Write to [support@ewars.ws](mailto:support@ewars.ws)
