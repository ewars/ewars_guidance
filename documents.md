# Documents

<strong>EWARS documents are pre-configured information products, used to reproduce the same analysis at regular intervals to support decision making. 
For example, this includes a Weekly Epidemiological Bulletin in the majority of EWARS deployments.
The design, layout and presentation of data in Documents is defined within Templates, that are available to Administrator.
</strong>

Notes:
- The design, layout and presentation of data in Documents is defined within Templates. You need Administrative privileges to be able to create Templates.

Related Guidance:
- What are the different user types
- [How to create templates](#/system-administration/10-templates) 

Table of contents   <br><br>
[1. How to access documents form the overview dashboard](#/documents/1-how-to-access-documents-from-the-overview-dashboard)   
[2. How to open Document Manager](#/documents/2-how-to-open-document-manager)   
[3. Viewing the full document history](#/documents/3-viewing-the-full-document-history)   
[4. Viewing recently published document](#/documents/4-viewing-recently-published-documents)   
[5. Sharing a document](#/documents/5-sharing-a-document)   
[6. Saving to PDF](#/documents/6-saving-to-pdf)   
[7. Downloading data from a document](#/documents/7-downloading-data-from-a-document)   
[8. Navigating documents](#/documents/8-navigating-documents)   
                       