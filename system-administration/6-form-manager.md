# Form Manager

Data collection forms can be designed and maintained using the Form Manager:

1. To open the Form Manager, go to the Main Menu ( ![image alt text](system-administration/image_66.png) )
2. Select Administration > Form Manager   
![image](system-administration/image_67.png)
<span class="coordinates">none;</span>
3. This will open the Form Manager