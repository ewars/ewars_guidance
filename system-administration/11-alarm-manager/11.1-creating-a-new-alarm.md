# Creating a new alarm

To create a new alarm, in the Alarm Manager:

1. Press the Add ( ![image alt text](system-administration/alarm-manager/image_39.png) ) button    
2. Click on + New Alarm     
![image](system-administration/alarm-manager/image_40.png)
<span class="coordinates">2,16,220,west;</span>

![main-image](system-administration/alarm-manager/image_41.png)
<span class="coordinates">1,12,14,west;</span>