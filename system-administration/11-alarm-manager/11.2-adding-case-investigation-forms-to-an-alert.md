# Adding case investigation forms to an alert

It is possible to associate specific case investigations to specific types of alerts.
<ol>
<li>Click on an alarm to open it from the menu on the left</li>
<li>In <strong>Investigations</strong>, you can configure:
<h4>Enabled:</h4>
<table>
  <tr>
    <td>Enable</td>
    <td><i>On | Off</i></td>
  </tr>
</table>
<h4>Investigation settings <strong>(when enabled)</strong>:</h4>
<table>
  <tr>
    <td>Associated forms</td>
    <td>Use the buttons to activate investigation forms and make them available during management of specific alerts</td>
  </tr>
  <tr>
    <td>Interval periods</td>
    <td>Specify the number of units before the alert is auto-dismissed</td>
  </tr>
</table>
</li>
<li>Press the Save ( <img src="system-administration/alarm-manager/image_50.png"> ) button to save your changes<br>
</li>
</ol>

![main-image](system-administration/alarm-manager/image_51.png)
<span class="coordinates">1,60,100,west;2,23,460,west;3,23,200,west;</span>

Notes:
- Only forms that are classified as investigative under Form Type will be available for selection under the Investigation Settings in Alarms

Related guidance:
- Adding form settings