# Add assignment details

In the Assignment Editor:

1. Select the Form the user requires permission to report
2. Enter the Location the user can report from
3. Select the Start Date of the Assignment
4. Select the End Date of the Assignment
5. Press Save Change(s) 

![main-image](system-administration/image_111.png)
<span class="coordinates">1,112,340,east;2,159,340,east;3,203,340,east;4,253,340,east;5,290,104,west;</span>