# Deleting EWARS accounts

In the Account Manager:

1. Select an Account from the list on the left 
2. Press the Delete ( ![image alt text](system-administration/image_7.png) ) button 

![main-image](system-administration/image_8.png)
<span class="coordinates">2,20, 240, west;1,170, 100, west;</span>
