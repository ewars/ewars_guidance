# Templates

You can access and manage Devices using the Report Template Manager:

1. To open the Report Template Manager, go to the Main Menu
( ![image alt text](system-administration/image_138.png) )
2. Select Administration > Report Templates
![image](system-administration/image_139.png)
<span class="coordinates">none;</span>
3. This will open the Report Template Manager