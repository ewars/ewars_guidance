# Alarm Manager

Alarms define the way in which alerts are generated and behave in EWARS. You can access and manage Alerts definitions using the Alarms Manager:

1. To open the Alarms Manager, go to the Main Menu 
( ![image alt text](system-administration/alarm-manager/image_37.png) )
2. Select Administration > Alarms   
![image](system-administration/alarm-manager/image_38.png)
<span class="coordinates">none;</span>
3. This will open the Indicator Manager  