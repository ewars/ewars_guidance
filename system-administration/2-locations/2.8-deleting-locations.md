# Deleting locations

If a location was created by mistake and needs to be deleted, you can reflect this EWARS by updating its status in the Location Settings.

In the Locations Manager:

1. Click on a location to open it
2. In **General Settings**, update the Status field to Deleted
3. Press the Save ( ![image alt text](system-administration/image_32.png) ) button to save your changes

![main-image](system-administration/image_33.png)
<span class="coordinates">1,110,150,west;2,150,491,east;3,25,253,west;</span>

Notes:
* Deleted locations will no longer appear in any of the selectable lists of Reporting Locations, nor in the statistics for Completeness of Reporting
