# Edit SMS Gateway settings

When you have opened the Edit SMS Gateway Screen:

1. Under Gateway Settings you can review the following settings

**Platform Details:**

<table>
  <tr>
    <td><strong>Name</strong></td>
    <td>Name of operating system on phone (non-editable)</td>
  </tr>
  <tr>
    <td><strong>Status</strong></td>
    <td>Unique device IMEI number (non-editable)</td>
  </tr>
  <tr>
    <td><strong>Account</strong></td>
    <td>Version of the operating system on phone (non-editable)</td>
  </tr>
  <tr>
    <td><strong>Can Receive</strong></td>
    <td>Select from list to determine if gateway can receive reports via SMS from a device:
Yes | No</td>
  </tr>
  <tr>
    <td><strong>Can Send</strong></td>
    <td>Select from list to determine if gateway can send SMS feedback to a device:
Yes | No</td>
  </tr>
  <tr>
    <td><strong>Receive #</strong></td>
    <td>Phone number SMS reports should be submitted to</td>
  </tr>
  <tr>
    <td><strong>Send #</strong></td>
    <td>Phone number SMS feedback will be sent from</td>
  </tr>
  <tr>
    <td><strong>Version</strong></td>
    <td>SMS Gateway Version Number</td>
  </tr>
</table>

