# Editing time series widget settings

In the Widget Editor

1. In the chart type toggle, select Time Series Chart   
![image alt text](system-administration/image_151.png)
2. In the General Settings, you can configure:

<h4>General Settings:</h4>
<table>
  <tr>
    <td>Chart Title</td>
    <td>Display title for chart</td>
  </tr>
  <tr>
    <td>Sample Interval</td>
    <td>Chart interval. 

Select from:
Daily | Weekly | Monthly | Annually</td>
  </tr>
</table>
<h4>Data Start Date:</h4>
<table>
  <tr>
    <td>Date Specification</td>
    <td>Define start date for chart. 

Select from:
Manual | Interval(s) back from End Date | Report Date 
</td>
  </tr>
</table>
<h4>Data End Date:</h4>

<table>
  <tr>
    <td>Date Specification</td>
    <td>Define end date for chart. 

Select from:
Manual | Interval(s) back from End Date | Report Date 
</td>
  </tr>
</table>
<h4>Controls:</h4>

<table>
  <tr>
    <td>Show Title</td>
    <td>Display chart title
Select On | Off</td>
  </tr>
  <tr>
    <td>Show Export</td>
    <td>Allow export data from chart
Select On | Off</td>
  </tr>
  <tr>
    <td>Zoomable</td>
    <td>Allow zoom into chart by click dragging
Select On | Off</td>
  </tr>
  <tr>
    <td>Show Navigator</td>
    <td>Show timeline navigator below chart
Select On | Off</td>
  </tr>
  <tr>
    <td>Show Legend</td>
    <td>Show legend
Select On | Off</td>
  </tr>
</table>
<h4>Style:</h4>

<table>
  <tr>
    <td>y-axis Label</td>
    <td>Enter label for y-axis</td>
  </tr>
  <tr>
    <td>y-axis format</td>
    <td>Select number format for y-axis</td>
  </tr>
  <tr>
    <td>Chart height</td>
    <td>Define chart height in px</td>
  </tr>
  <tr>
    <td>Chart widget</td>
    <td>Define chart width in px</td>
  </tr>
  <tr>
    <td>x-axis label rotation</td>
    <td>Add number of degrees for x-axis rotation</td>
  </tr>
</table>

![main-image](system-administration/image_152.png)
<span class="coordinates">1,-8,263,west;2,128,423,west;</span>