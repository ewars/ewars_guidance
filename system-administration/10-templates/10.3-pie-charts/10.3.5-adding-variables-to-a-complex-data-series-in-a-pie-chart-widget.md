# Adding variables to a complex data series in a pie chart widget

To add new variables to a complex data series, in the Complex Series Editor:

1. Press Add Variable ( ![image alt text](system-administration/image_178.png) ) to add a complex variable to the editor   
2. Press the Edit ( ![image alt text](system-administration/image_179.png) ) button   
3. In the Complex Variable Editor, you can configure:
![image](system-administration/image_180.png)
<span class="coordinates">3,50,316,west;</span>

<h4>Complex Source:</h4>

<table>
  <tr>
    <td>Variable Name</td>
    <td>Enter a title for the complex variable</td>
  </tr>
  <tr>
    <td>Location</td>
    <td>Define location for the complex data variable</td>
  </tr>
  <tr>
    <td>Indicator Source</td>
    <td>Select the indicator source of the complex data variable</td>
  </tr>
</table>


![main-image](system-administration/image_181.png)
<span class="coordinates">1,546,606,east;2,606,580,east;</span>

Notes:
* The variable name must match those used in the formula for the Complex Series
    **Language settings**
