# Adding form settings

In the Form Template Manager:
<ol>
<li>Click the General Tab ( <img alt="image alt text" src="system-administration/image_70.png"/> )</li>

<li>In <strong>General Form Settings</strong>, you can configure:

<h4>Location Info:</h4>
<table>
  <tr>
    <td>Form Name</td>
    <td>Name of the indicator</td>
  </tr>
  <tr>
    <td>Form Type</td>
    <td>Select from the list:
Active | Inactive</td>
  </tr>
  <tr>
    <td>Reporting Interval</td>
    <td>Select the group where the indicator belongs from the list</td>
  </tr>
  <tr>
    <td>Account</td>
    <td>Text description of the indicator</td>
  </tr>
  <tr>
    <td>Location aware</td>
    <td></td>
  </tr>
  <tr>
    <td>Block Future Dates?</td>
    <td></td>
  </tr>
  <tr>
    <td>Lab Access</td>
    <td></td>
  </tr>
  <tr>
    <td>Form Status</td>
    <td></td>
  </tr>
  <tr>
    <td>Form Description</td>
    <td></td>
  </tr>
</table>
</li>
</ol>
![main-image](system-administration/image_71.png)
<span class="coordinates">1,21,71,west;2,121,391,west;</span>