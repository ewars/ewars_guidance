# Organizations

Organizations are managed using the EWARS Accounts Manager:
1. To open the EWARS Accounts Manager, go to the Main Menu ( ![alt](system-administration/image_0.png) )
2. Select Administration > Accounts Manager   
![image](system-administration/image_1.png)
<span class="coordinates">none;</span>
3. This will open the Accounts Manager