# Locations

There are broadly two types of location within EWARS:
<div class="no-line-height">
    <p class="lh-reset"><h3>Administrative locations</h3><br>These define the administrative hierarchy within a country (e.g. Country > Region > District). They are defined by a geographic area and are represented on a map by a polygon. The initial setup of Administrative Locations within a new EWARS Account will normally be the responsibility of the Global Administrator. In future, functionality is planned to allow Account Administrators to import Administrative locations directly from existing available sources (e.g. OCHA Common Operational Datasets (CODs)).In the event that Administrative Locations change or are altered during a deployment, then Account administrators have the ability to edit polygons directly in the application. Administrative locations can also include refugee and IDP camps, within which health services are provided.
    </p>
    <p class="lh-reset"><h3>Point locations</h3><br>These are typically hospitals and health facilities that are responsible for submitting reports within EWARS. They may include Ministry of Health (MoH) facilities, as well as those operated by NGOs or the private sector. They can be fixed or mobile facilities; and can also include temporary sites setup during a response (e.g. oral rehydration points (ORPs), cholera treatment centres (CTCs)). If a comprehensive inventory of health facilities within a country exists, then this can be imported into EWARS when a new account is created by the Global Administrator. Location Administrator are responsible for maintaining of these locations, and for opening or closing of sites within their areas of responsibility during a deployment. Account Administrators can also help provide support for this.
    </p>
</div>

You can access and manage locations using the Locations Manager:

1. To open the Locations Manager, go to the Main Menu ( ![image alt text](system-administration/image_9.png) )
2. Select Administration > Locations    
![main-image](system-administration/image_10.png)
<span class="coordinates">none</span>
3. This will open the Locations Manager