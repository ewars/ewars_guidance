# Device Management

You can access and manage Devices using the Device Manager:

1. To open the Device Manager, go to the Main Menu ( ![image alt text](system-administration/image_128.png) )
2. Select Administration > Devices
![image](system-administration/image_129.png)
<span class="coordinates">none;</span>
3. This will open the Device Manager