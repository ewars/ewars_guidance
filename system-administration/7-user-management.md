# User Management
Individual User Accounts can be managed using the User Manager:
1. To open the User Manager, go to the Main Menu ( ![image alt text](system-administration/image_86.png) )
2. Select Administration > User Manager
![image](system-administration/image_87.png)
<span class="coordinates">none;</span>
3. This will open the Form Manager