# Editing indicator guidance

In the Indicator Manager:

1. Click on a location to open it from the menu on the left
2. In **Guidance**, you can add additional guidance notes on individual indicators for users
3. Use the buttons at the top of the guidance box to format the text
![image alt text](system-administration/image_62.png)
4. Press the Save ( ![image alt text](system-administration/image_63.png) ) button to save your changes   

![main-image](system-administration/image_64.png)
<span class="coordinates">1,131,132,west;2,191,442,east;3,85,712,west;4,25,202,west;</span>