# Creating new indicators

In the Indicator Manager:

1. Press the Add ( ![image alt text](system-administration/image_55.png) ) button
2. Click on + New Indicator   
![image](system-administration/image_56.png)
<span class="coordinates">2,72,252,west;</span>

![main-image](system-administration/image_57.png)
<span class="coordinates">1,11,32,west;</span>