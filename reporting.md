# Reporting

<strong>EWARS supports the reporting of data for surveillance or response. This can be regular, ongoing reporting (for example, daily or weekly) or based on ad hoc reporting (for example within event-based surveillance, or for case-based investigations).
</strong>   

Table of contents   <br><br>
[1. Surveillance Dashboard](#/reporting/1-surveillance-dashboard)   
[2. Report Manager](#/reporting/2-report-manager)   
[3. Creating a new report](#/reporting/3-creating-a-new-report)   
[4. Deleting reports](#/reporting/4-deleting-report)   
[5. Editing reports after submission](#/reporting/5-editing-reports-after-submission)   
