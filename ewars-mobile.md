# EWARS Mobile

<strong>EWARS is committed to supporting disease surveillance, alert and response even in the most difficult and remote operating environments.
EWARS Mobile is a mobile application available on Android OS. It allows data to collected offline, and then synchronised with the online EWARS application when a connection is available. Data can be synchronised by SMS or via a data connection (e.g. GPRS, 3G).
An EWARS account and assignments are needed prior to being able to login to EWARS Mobile. 
</strong>   

Table of contents   <br><br>
[1. Configuration of Android Phone](#/ewars-mobile/1-configuration-of-android-phone)   
[2. Installation from Google Play](#/ewars-mobile/2-installation-from-google-play)    
[3. Installation from local file](#/ewars-mobile/3-installation-from-local-file)   
[4. Signing into ewars mobile](#/ewars-mobile/4-signing-into-ewars-mobile)   
[5. Using EWARS Mobile](#/ewars-mobile/5-using-ewars-mobile)    
[6. Synchronising EWARS Mobile](#/ewars-mobile/6-synchronising-ewars-mobile)   
[7. Additional tips](#/ewars-mobile/7-additional-tips)   
                                