# How to access documents from the overview dashboard

To open Documents: 

1. Go to the My Documents widget on the Overview Dashboard:

![main-image](documents/image_54.png)
<span class="coordinates">1,177,519,east;</span>