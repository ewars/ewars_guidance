# Downloading data from a document

It is possible to download data contained within the charts and tables within a document:

1. Open the document in the browser
2. Click on the Download Data button ( ![image alt text](documents/image_66.png) )

![main-image](documents/image_67.png)
<span class="coordinates">1,240,704,west;2,14,707,east;</span>

Notes:
- The document will save as a .csv file in  your downloads folder
- You can then open in applications such as MS Excel. The data for each chart will be stored on a separate worksheet.
