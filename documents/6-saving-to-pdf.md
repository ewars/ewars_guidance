# Saving to PDF

To save a Document as a pdf file:

1. Open the document in the browser
2. Click on the PDF button ( ![image alt text](documents/image_64.png) )

![main-image](documents/image_65.png)
<span class="coordinates">1,240,704,west;2,14,637,east;</span>

Notes:
* The document will save as a PDF file in  your downloads folder
