# Sharing a document

To share a Document:

1. Open the document in the browser
2. Click on the Share ( ![image alt text](documents/image_60.png) ) button   
3. In the Share Document box, enter the email address(es) of the colleague(s) you would like to share the document with
![image](documents/image_61.png)
<span class="coordinates">3,116,670,west;</span>
4. Press Share    
![image alt text](documents/image_62.png)

![main-image](documents/image_63.png)
<span class="coordinates">1,220,700,west;2,15,670,east;</span>