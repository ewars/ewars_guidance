# How to open Document Manager

You can also view Documents by:

1. Go to the Main Menu ( ![image alt text](documents/image_55.png) )
2. Select Analysis > Documents
![image](documents/image_56.png)
<span class="coordinates">none</span>
