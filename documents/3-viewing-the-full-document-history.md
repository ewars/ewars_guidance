# Viewing the full document history

To view the full history of all reports published for a specific Document:

1. Go to Browse Documents
2. Click on the Document of interest
3. This will show the full list of reports available for the Document 
![image](documents/image_57.png)
<span class="coordinates">3,106,532,west;4,232,573,west;</span>
4. Click on a Document to open it in the browser

![main-image](documents/image_58.png)
<span class="coordinates">1,-16,3,south;2,58,203,west;</span>
