# New user assignments

To approve new User Assignment requests:

1. Go to the Tasks dashboard 
2. Click on the User Assignment Request in the list
3. In the Assignment Request form, review the Form, Location and Dates that have been requested 
![image](task-management/image_185.png)
<span class="coordinates">3,-10,244,west;</span>
4. Press the Approve or Reject ( ![alt image](task-management/image_186.png) ) button to make your choice
5. The user will be sent an email to inform them of the outcome

![main-image](task-management/image_187.png)
<span class="coordinates">1,20,104,west;2,75,574,west;</span>

Notes:
- All new user assignment requests can only be approved by an Administrator. If you are unsure about what type of assignments a user should have, you should write to them separately to confirm the details are correct.
- User assignments should normally be approved by a Location Administrator, who works at an Administrative level close to the location where the assignment has been requested.

Related guidance:
- [Types of user accounts](#/user-accounts/1-types-of-user-accounts) 
- [Signing up for a new user account](#/user-accounts/3-sign-up-for-a-new-account) 
- [Requesting a new assignment](#/assignments/1-requesting-a-new-assignment) 
