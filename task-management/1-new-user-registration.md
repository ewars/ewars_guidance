# New user registration

To approve new User Registration requests:

1. Go to the Tasks dashboard 
2. Click on the User Account Request in the list
3. This will open the Registration Request form, where you can confirm the Name, Email and User Type details are correct
![image](task-management/image_182.png)
<span class="coordinates">3,-10,244,west;</span>
4. Press the Approve or Reject ( ![image alt text](task-management/image_183.png) ) button to make your choice
5. The user will be sent an email to inform them of the outcome

![main-image](task-management/image_184.png)
<span class="coordinates">1,20,104,west;2,107,434,west;</span>

Notes:
- All new user registration requests can only be approved by an Account Administrator. 
- If you are unsure about what type of account a user should have, you should write to them separately to confirm the details are correct.

Related guidance:
- [Types of user accounts](#/user-accounts/1-types-of-user-accounts) 
- [Signing up for a new user account](#/user-accounts/3-sign-up-for-a-new-account) 