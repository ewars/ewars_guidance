# Report deletion requests

To approve Amendment Requests:

1. Go to the Tasks dashboard
2. Click on the Deletion Request in the list
3. In the Deletion Request, review details of the Form, the Proposed Changes and the Reason for the Amendment.
![image](task-management/image_191.png)
<span class="coordinates">3,-10,244,west;</span>
4. Press the Approve or Reject ( ![image alt text](task-management/image_192.png) ) button to make your choice
5. The user will be sent an email to inform them of the outcome

![main-image](task-management/image_193.png)
<span class="coordinates">1,-7,104,west;2,140,384,west;</span>

Notes:
- All report deletion requests should normally be approved by a Location Administrator, who works at an Administrative level close to the staff responsible for reporting and can verify the reasons for deleting the report.

Related guidance:
- [Types of user accounts](#/user-accounts/1-types-of-user-accounts) 
- [Deleting reports](#/reporting/4-deleting-reports) 
