# Surveillance Dashboard

To be able to submit data you must first have requested and been granted an Assignment, to be able to report for a given form in a specific location.

Related Guidance:
- [Requesting a new assignment](#/assignments/1-requesting-a-new-assignment)  
