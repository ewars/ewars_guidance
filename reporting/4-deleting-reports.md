# Deleting reports 

To delete a report:

1. Open the report in the Report Viewer. This will show you the User who submitted the Report and the Date it was submitted
2. Press the Delete Report button
3. Add a reason into the Delete Report box
![image](reporting/image_35.png)
<span class="coordinates">3,106,161,west;</span>
4. Press the Delete ( ![image alt text](reporting/image_36.png) ) button 

Notes: 
- Requests to delete reports are not automatically accepted in EWARS and must be approved by an Administrator. 
- Once an deletion request has been made, further attempts to delete the same report cannot be made until the pending request has been approved or rejected. 

Related guidance:
- [Managing report deletion requests](#/task-management/4-report-deletion-requests) 

![main-image](reporting/image_37.png)
<span class="coordinates">1,-10,531,east;2,-7,161,west;</span>
