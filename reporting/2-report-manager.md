# Report Manager

You can also access and manage reports using the Report Manager:

1. To open the Report Manager, go to the Main Menu ( ![image alt text](reporting/image_17.png) )
2. Select Data Collection > Report Manager
![image](reporting/image_18.png)
<span class="coordinates">none<span>
