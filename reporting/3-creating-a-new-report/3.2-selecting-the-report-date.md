# Selecting the report date 

To enter the report date:

1. Click on the calendar icon in the Report Date field
2. Select the applicable reporting interval from the Reporting Calendar    
![image](reporting/image_26.png)
<span class="coordinates">2,110,200,west;</span>

![main-image](reporting/image_27.png)
<span class="coordinates">1,210,690,east;</span>

Notes:
* If you open a report from Upcoming or Overdue list of reports in the Surveilance Dashboard, the report date will automatically be entered.
* Certain forms cannot be submitted for dates in the future. In this case, the report cannot be submitted until the date has been exceeded.
* In this case, upcoming reports with a pre-filled date in the future will be marked in red:
![image alt text](reporting/image_28.png)
* Future dates will also be marked in red in the Reporting Calendar and will not be able to be selected
