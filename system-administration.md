# System Administration

<strong>System administrators need higher level views of the application, in order to be able to configure, establish and manage the system at country-level.
EWARS contains a number of administrative features, available to high-level Global or Account Administrators, to be able to effectively manage these functions.
</strong>   

Table of contents   <br><br>
[1. EWARS Accounts](#/system-administration/1-ewars-accounts)   
[2. Locations](#/system-administration/2-locations)   
[3. Organizations](#/system-administration/3-organizations)   
[4. Laboratories](#/system-administration/4-laboratories)   
[5. Indicators](#/system-administration/5-indicators)   
[6. Form Manager](#/system-administration/6-form-manager)   
[7. User Management](#/system-administration/7-user-management)   
[8. SMS Gateways](#/system-administration/8-sms-gateways)   
[9. Device Management](#/system-administration/9-device-management)   
[10. Templates](#/system-administration/10-templates)   
[11. Alarm manager](#/system-administration/11-alarm-manager)    