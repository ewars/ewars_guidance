# Viewing active alerts

To view active alerts:

1. Go to the Alert Dashboard 
2. Select the Active tab in the menu   
![image alt text](alert/image_1.png)
3. The list will show all active alerts in EWARS, grouped by type of alert and listed by date. Click an alert to load it in the Alert Log
4. The map will display the location of each alert. Click on a map symbol to view the details of the alert.   
![image alt text](alert/image_2.png)
5. Click the more info to load it in the Alert Log

![main-image](alert/image_3.png)
<span class="coordinates">1,100,59,west;2,61,551,east;3,167,775,west;4,167,471,west;</span>