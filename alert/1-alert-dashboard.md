# Alert Dashboard

The alert dashboard provides an overview of all alerts that are active and inactive in EWARS.

To open the alert dashboard:
1. Click on the Alert Dashboard

![main-image](alert/image_0.png)
<span class="coordinates">1,99,59,west;</span>