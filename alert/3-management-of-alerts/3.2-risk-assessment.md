# Risk Assessment 

If an alert has been verified and a decision made to conduct a risk assessment, then the Risk Assessment stage will become active. 

1. Click on the Start Risk Assessment ( ![alt image](alert/image_16.png) ) button   
2. In the Risk Assessment form, enter information to characterize the hazard, exposure and context   
![image](alert/image_17.png)
<span class="coordinates">2,230,550,west;</span>
3. Press Submit    
![image](alert/image_18.png)
<span class="coordinates">3,6,120,west;</span>

![main-image](alert/image_19.png)
<span class="coordinates">1,262,708,west;</span>

Notes:
- You can access more guidance Risk Assessment Form by toggling the guidance button:
![image alt text](alert/image_20.png)
