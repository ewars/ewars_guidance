# Alert Log

In addition to viewing alerts in the Alert Dashboard, you can also access detailed information on each alert, and contribute to the active management of an alert, using the Alert Log.

1. To open the Alert Log, go to the Main Menu ( ![image alt text](alert/image_7.png) )
2. Select Data Collection > Alert Log
![main](alert/image_8.png)
<span class="coordinates"></span>