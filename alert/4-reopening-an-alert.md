# Re-opening an alert

After a final outcome has been assigned to an alert, it will be classified as inactive. However, if new information becomes available, or the level of risk changes, then this may make it necessary to re-open an alert and update information in the risk assessment.

To re-open an alert:
1. Click on the Re-open alert button   
![images s](alert/image_31.png)
2. Enter a reason for why the alert is being re-opened    
![image](alert/image_32.png)
<span class="coordinates">2,114,131,west;</span>
3. The alert will be re-opened allowing updates to be made to the verification and risk assessment stages    

![main-image](alert/image_33.png)
<span class="coordinates">1,114,131,west;</span>

Notes:
- If the alert was discarded at the verification step, then it will be re-opened at this stage to allow a change in the verification decision.   
- If an alert was discarded, or set to respond, at the final outcome step then it will be re-opened at the Risk Assessment stage, to allow updates to the risk assessment, risk characterization and final outcome stages. 