# Management of alerts

When you open an alert in the alert log, you will see the detailed Alert Management screen. This shows you:
1. A header area with name, location, type and source of the alert
2. The alert workflow area, showing the current alert state and the pending actions 
3. An information area, where you can click on the tabs to display an activity feed, the graph of the alert, the report that triggered the alert, users who are invited to contribute to the management of the alert
![image alt text](alert/image_9.png)

Related guidance:
- Configuring alert thresholds 

![main-image](alert/image_10.png)
<span class="coordinates">1,56,500,west;2,110,79,west;3,325,300,west;</span>